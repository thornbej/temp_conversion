use std::io;

fn main() {
    
    let mut base_temp_type = String::new();
    
    //can only accept the values 'c' and 'f' so putting it into a loop
    loop {
    println!("Please enter celcius (c) or farenheit (f):");
    io::stdin().read_line(&mut base_temp_type)
       .expect("Failed to read line");
    
    if base_temp_type.trim() == "c" {
      break;  
       }
    
    if base_temp_type.trim() == "f" {
    break;    
       }   
    //clear the variable otherwise on the next iteration it will simply add the value to the
    //string! :(
    base_temp_type.clear();
    }
    
    println!("You selected: {}", base_temp_type);
    let base_temp_type = base_temp_type.to_string();
    
    println!("please enter your temperature to be converted:");
    let mut temp = String::new();
    io::stdin().read_line (&mut temp)
        .expect ("failed to read that!");
    println!("you entered: {}", temp);
    let temp = temp.trim().parse::<f64>().unwrap_or(0.0);

    if base_temp_type.trim() == "c" 
        {convertf(temp); 
    } 
    else {convertc(temp);
    }

fn convertf(temp: f64) {
    //maths could be cleaned up here using consts declared outside fn main()
    let conv_temp = (((temp*9.0)/5.0)+32.0);
    println!("that would make the temperature {} in Farenheit!", conv_temp);
    }

fn convertc(temp: f64)
   {
     //again maths could be cleared up here using consts declared outside fn main()  
     let conv_temp = (((temp-32.0)*5.0)/9.0);
       println!("that would make the temperature {} in celcius", conv_temp);
   }
}:
